import React from "react";
import "@lottiefiles/lottie-player";

export default class LottieControl extends React.Component {
  render() {
    return (
      <div>
        <lottie-player
          autoplay
          controls
          loop
          mode="normal"
          src="https://assets9.lottiefiles.com/packages/lf20_lQjPRg.json"
          style={{width: 700}}
        ></lottie-player>
      </div>
    );
  }
}
